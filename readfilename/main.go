package readfilename

import (
	"fmt"
	"os"
)

//获取指定文件夹内的文件名
func ReadFileDir(FPath string, n int) []string {
	o, err := os.Open(FPath)
	if err != nil {
		fmt.Println(err)
	}
	defer o.Close()
	strl, err := o.Readdirnames(n)
	if err != nil {
		fmt.Println(err)
	}
	return strl
}
